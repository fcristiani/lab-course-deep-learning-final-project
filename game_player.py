import numpy as np
from utils import Options
from random import randrange

class GamePlayer:
	def __init__(self, playerId):
		self.position_old_buffer = np.zeros(2, dtype=int)
		self.position_old = np.zeros(2, dtype=int)
		self.position = np.zeros(2, dtype=int)
		self.id = playerId
		self.opt = Options()

	def takeAction(self, state=None, randomProbability=0):
		return randrange(self.opt.act_num)

	def applyAction(self, actionVector):
		self.position_old_buffer = self.position_old
		self.position_old = self.position
		self.position = self.position + actionVector

	def revertAction(self):
		self.position = self.position_old
		self.position_old = self.position_old_buffer

	def samePositionAs(self, anotherGamePlayer):
		return (self.position[0] == anotherGamePlayer.position[0] and self.position[1] == anotherGamePlayer.position[1])

	def train(self, transitionTable):
		pass

	def setSession(self, session):
		pass