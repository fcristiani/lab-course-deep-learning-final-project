import numpy as np

class SimulatorDrawer:
	def __init__(self, simulator):
		self.simulator = simulator

	def draw_step(self):
		# erase old bot & draw new bot
		# black old bot
		# blue new bot
		for bot in self.simulator.bots:
			self.draw_cube(
				bot.position_old[0],
				bot.position_old[1],
				self.simulator.bot_clr_ind,
				0
			)

			self.draw_cube(
				bot.position[0],
				bot.position[1],
				self.simulator.bot_clr_ind,
				255
			)

		self.draw_cube(
			self.simulator.target.position_old[0],
			self.simulator.target.position_old[1],
			self.simulator.tgt_clr_ind,
			0
		)

		self.draw_cube(
			self.simulator.target.position[0],
			self.simulator.target.position[1],
			self.simulator.tgt_clr_ind,
			255
		)

	def draw_cube(self, y, x, clr_ind, clr_val):
		# draw on channel clr_ind with clr_val
		y1, y2, x1, x2 = self.get_cube_from_ind(y, x)
		self.simulator.state_screen[y1:y2, x1:x2, clr_ind] = clr_val

	def draw_reset(self):
		self.simulator.state_screen = np.zeros(
			(self.simulator.map_hei*self.simulator.cub_siz, self.simulator.map_wid*self.simulator.cub_siz, 3),
			dtype=np.uint8
		)

		for obj_ind in range(self.simulator.obs_ind, self.simulator.obj_num):
			self.draw_cube(
			self.simulator.obj_pos[obj_ind][0],
			self.simulator.obj_pos[obj_ind][1],
			self.simulator.obs_clr_ind,
			255
		)

	def draw_new(self):
		# erase old bot tgt & draw new tgt

		# black old bot

		for bot in self.simulator.bots:
			if bot.position_old[0] != self.simulator.map_hei and bot.position_old[1] != self.simulator.map_wid:
				self.draw_cube(
					bot.position_old[0],
					bot.position_old[1],
					self.simulator.bot_clr_ind, 0
				)

		# black old tgt
		if self.simulator.target.position_old[0] != self.simulator.map_hei and self.simulator.target.position_old[1] != self.simulator.map_wid:
			self.draw_cube(
				self.simulator.target.position_old[0],
				self.simulator.target.position_old[1],
				self.simulator.tgt_clr_ind,
				0
			)

		# green new tgt
		self.draw_cube(
			self.simulator.target.position[0],
			self.simulator.target.position[1],
			self.simulator.tgt_clr_ind,
			255
		)

	def draw_pob(self):
		# crop pob from screen

		self.simulator.state_pob = []

		for bot in self.simulator.bots:
			y1, y2, x1, x2 = self.get_pob_from_ind(
				bot.position[0], bot.position[1]
			)
			self.simulator.state_pob.append(self.simulator.state_screen[y1:y2, x1:x2, :])

		y1, y2, x1, x2 = self.get_pob_from_ind(
			self.simulator.target.position[0], self.simulator.target.position[1]
		)
		self.simulator.state_pob.append(self.simulator.state_screen[y1:y2, x1:x2, :])

	def get_cube_from_ind(self, y, x):
		return self.simulator.cub_siz*y, self.simulator.cub_siz*(y+1), self.simulator.cub_siz*x, self.simulator.cub_siz*(x+1)

	def get_pob_from_ind(self, y, x):
		pob_edg = self.simulator.pob_siz // 2

		return (
			self.simulator.cub_siz*(y-pob_edg),
			self.simulator.cub_siz*(y+pob_edg+1),
			self.simulator.cub_siz*(x-pob_edg),
			self.simulator.cub_siz*(x+pob_edg+1)
		)

	def get_h_val(self, active_pose, tgt_y, tgt_x):
		return np.abs(active_pose[0] - tgt_y) + np.abs(active_pose[1] - tgt_x)
