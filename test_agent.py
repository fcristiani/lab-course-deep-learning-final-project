from simulation_runner import SimulationRunner
from utils import Options
from dqn_game_player import DQNGamePlayer
from game_player import GamePlayer
from stay_still_game_player import StayStillGamePlayer
from keyboard_input_game_player import KeyboardInputGamePlayer
import tensorflow as tf
import os

opt = Options()
sess = tf.Session()

bots = []

for x in range(0, opt.number_of_bots):
    bot = DQNGamePlayer("Bot_{}".format(x + 1))
    bot.setSession(sess)
    bots.append(bot)

# bots[0] = KeyboardInputGamePlayer("Bot_1")

target = StayStillGamePlayer("Target_1")

sess.run(tf.global_variables_initializer())

if os.path.exists("./model") is True:
    print("Restoring model ...")
    saver = tf.train.Saver()
    saver.restore(sess, "./model/")

runner = SimulationRunner(bots, target)
accuracy = runner.evaluate(100, enableDrawing=True)

print("\nFinal accuracy: {0:.2f}%".format(accuracy))