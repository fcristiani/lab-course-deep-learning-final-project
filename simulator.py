import numpy as np
from random import randrange
# custom modules
from utils import State
from maps import maps
from simulator_drawer import SimulatorDrawer

class Simulator:

    # basic funcs

    def __init__(self, map_ind, cub_siz, pob_siz, act_num, bots, target):
        self.map_ind = map_ind
        self.cub_siz = cub_siz
        self.pob_siz = pob_siz
        self.bot_ind = 0 # bot's index in obj_pos
        self.tgt_ind = 1 # target's index in obj_pos
        self.obs_ind = 2 # object's index in obj_pos
        self.bot_clr_ind = 2 # blue
        self.tgt_clr_ind = 1 # green
        self.obs_clr_ind = 0 # red
        # state
        self.state_dim = 2
        # action
        self.act_dim   = 2 # 0: ^V; 1: <>
        self.act_num   = act_num # {o, ^, V, <, >}
        self.act_pos_ind = np.array([
            [ 0,  0], # o
            [-1,  0], # ^
            [ 1,  0], # V
            [ 0, -1], # <
            [ 0,  1]  # >
        ])

        self.target = target
        self.bots = bots

        self.drawer = SimulatorDrawer(self)

        self.reset_map(self.map_ind)

    def __del__(self):
        print("Garbage collected.")

    # reset funcs

    def reset_map(self, map_ind):
        self.map     = maps[self.map_ind]
        self.map_hei = self.map.shape[0]
        self.map_wid = self.map.shape[1]
        # parse map file
        obs_num = np.sum(self.map)

        self.obj_num = len(self.bots) + 1 + obs_num  # all bots + 1 target + #obs
        self.fre_pos = np.ndarray((self.map_hei * self.map_wid - obs_num, self.state_dim), int) # free locations: candidates for bot & tgt
        self.obj_pos = np.ndarray((self.obj_num, self.state_dim), int) # keep track of all objects, including bot tgt & obs

        for bot in self.bots:
            bot.position[0] = self.map_hei
            bot.position[1] = self.map_wid

        self.target.position[0] = self.map_hei
        self.target.position[1] = self.map_wid
        obj_ind = self.obs_ind
        fre_ind = 0
        for y in range(self.map_hei):
            for x in range(self.map_wid):
                if self.map[y][x] == 1:
                    self.obj_pos[obj_ind][0] = y
                    self.obj_pos[obj_ind][1] = x
                    obj_ind += 1
                else:
                    self.fre_pos[fre_ind][0] = y
                    self.fre_pos[fre_ind][1] = x
                    fre_ind += 1
        self.reset_state()
        self.drawer.draw_reset()

    def reset_state(self):
        self.state_action   = 0
        self.state_reward   = 0.
        self.state_screen   = np.zeros((self.map_hei*self.cub_siz, self.map_wid*self.cub_siz, 3), dtype=np.uint8)
        self.state_terminal = False
        self.state_pob      = np.zeros((self.pob_siz*self.cub_siz, self.pob_siz*self.cub_siz, 3), dtype=np.uint8)
        return self.get_state()

    def act(self, gamePlayer):
        gamePlayer.applyAction(self.act_pos_ind[self.state_action, :])

        if self.collisionWithWallHappened(gamePlayer):
            self.state_reward   = -1.
            self.state_terminal = False
            gamePlayer.revertAction()
            return

        if self.collisionWithPlayerHappened(gamePlayer):
            self.state_reward   = -0.08
            self.state_terminal = False
            gamePlayer.revertAction()
            return

        if self.targetReached():
            self.state_reward   = 1.
            self.state_terminal = True
        else:
            self.state_reward   = -0.04
            self.state_terminal = False

    def targetReached(self):
        tgt_position = self.target.position

        for bot in self.bots:
            if ((tgt_position[0] == bot.position[0]) and (tgt_position[1] == bot.position[1])):
                return True

        return False

    def collisionWithWallHappened(self, gamePlayer):
        if (self.map[gamePlayer.position[0]][gamePlayer.position[1]] == 1):
            return True

    def collisionWithPlayerHappened(self, gamePlayer):
        for bot in self.bots:
            if gamePlayer is not bot:
                if gamePlayer.samePositionAs(bot):
                    return True

        return False

    def get_state(self):
        return State(self.state_action,
                     self.state_reward,
                     self.state_screen,
                     self.state_terminal,
                     self.state_pob)
    
    # interfacing funcs

    def newGame(self, tgt_y, tgt_x):
        # 0. setting up
        for bot in self.bots:
            if bot.position[0] != -1 and bot.position[1] != -1:
                bot.position_old[0] = bot.position[0]
                bot.position_old[1] = bot.position[1]

        if self.target.position[0] != -1 and self.target.position[1] != -1:
            self.target.position_old[0] = self.target.position[0]
            self.target.position_old[1] = self.target.position[1]

        # 1. assign tgt position
        if tgt_y != None and tgt_x != None:
            self.target.position[0] = tgt_y
            self.target.position[1] = tgt_x
        else:
            choose_tgt_ind = randrange(self.fre_pos.shape[0])
            self.target.position[0] = self.fre_pos[choose_tgt_ind][0]
            self.target.position[1] = self.fre_pos[choose_tgt_ind][1]

        # 2. assign bot position
        for bot in self.bots:
            choose_bot_ind = randrange(self.fre_pos.shape[0])
            bot.position[0] = self.fre_pos[choose_bot_ind][0]
            bot.position[1] = self.fre_pos[choose_bot_ind][1]

        # 3. wrap up
        self.drawer.draw_new()
        self.target.position_old[0] = self.target.position[0]
        self.target.position_old[1] = self.target.position[1]
        return self.step_all_players(0)

    def step_all_players(self, action):
        state = self.step(action, self.target)

        for bot in self.bots:
            state = self.step(action, bot)

        return state

    def step(self, action, gamePlayer):
        self.state_action = action
        self.state_reward   = -0.04
        self.state_terminal = False
        self.act(gamePlayer)
        self.drawer.draw_step()
        self.drawer.draw_pob()
        return self.get_state()
