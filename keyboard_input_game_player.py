from game_player import GamePlayer

class KeyboardInputGamePlayer(GamePlayer):
	def takeAction(self, state=None, randomProbability=0):
		action_key = input("Provide action for player: {}".format(self.id))

		action_map = {
			"w": 1,
			"s": 2,
			"a": 3,
			"d": 4
		}

		if action_key in action_map:
			action = action_map[action_key]
		else:
			action = 0

		return action