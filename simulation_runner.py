import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import time

from utils import Options, rgb2gray
from transitionTable import TransitionTable
from simulator import Simulator

class SimulationRunner:
	def __init__(self, bots, target):
		self.opt = Options()

		self.win_all = None
		self.win_pob = None

		self.simulatorTrain = Simulator(
			self.opt.map_ind,
			self.opt.cub_siz,
			self.opt.pob_siz,
			self.opt.act_num,
			bots,
			target
		)

		self.simulatorEvaluate = Simulator(
			self.opt.map_ind,
			self.opt.cub_siz,
			self.opt.pob_siz,
			self.opt.act_num,
			bots,
			target
		)

		self.gamePlayers = []

		for bot in bots:
			self.gamePlayers.append(bot)

		self.gamePlayers.append(target)

	def evaluate(self, nepisodes_to_run, enableDrawing=False):
		epi_step = 0
		nepisodes = 1
		nepisodes_solved = 0

		(state, state_with_history, next_state_with_history) = self.initializeGameTraining(self.simulatorEvaluate)

		print("Evaluating ...")
		while(nepisodes < nepisodes_to_run):
			for i, gamePlayer in enumerate(self.gamePlayers):
				if state.terminal or epi_step >= self.opt.early_stop:
					nepisodes += 1
					epi_step = 0

					if state.terminal:
						nepisodes_solved += 1

					(state, state_with_history, next_state_with_history) = self.initializeGameTraining(self.simulatorEvaluate)

				if gamePlayer is not self.simulatorEvaluate.target:
					networkInput = self.getNetworkInput(i, state_with_history, len(self.simulatorEvaluate.bots))
				else:
					networkInput = None

				action = gamePlayer.takeAction(networkInput)
				state = self.simulatorEvaluate.step(action, gamePlayer)

				print(
					"\tEvaluating episode: {0} / {1} ({2:.2f}%) - Epi step: {3} - Player: {4} - Action: {5}".format(
						nepisodes,
						nepisodes_to_run,
						(nepisodes / nepisodes_to_run) * 100,
						epi_step,
						gamePlayer.id,
						action
					)
				)

				self.append_to_hist(
					state_with_history[i],
					rgb2gray(state.pob[i]).reshape(self.opt.state_siz)
				)

				if enableDrawing:
					self.drawGame(state)

				epi_step += 1

		return self.getAccuracy(nepisodes, nepisodes_solved)

	def train(self, nepisodes_to_run):
		transitionTables = self.getTransitionTables()

		epi_step = 0
		nepisodes = 1
		nepisodes_solved = 0
		epsilon = 20

		(state, state_with_history, next_state_with_history) = self.initializeGameTraining(self.simulatorTrain)

		print("Training ... | {} episodes".format(nepisodes_to_run))

		self.startProgress = time.time()

		lastLossValues = np.zeros(len(self.simulatorTrain.bots))

		step = 0
		while(nepisodes < nepisodes_to_run + 1):
			self.printProgess(step, nepisodes, nepisodes_to_run, lastLossValues)

			for i, gamePlayer in enumerate(self.gamePlayers):
				if state.terminal or epi_step >= self.opt.early_stop:
					nepisodes += 1
					epi_step = 0
					self.checkTrainingProgress(nepisodes)

					if state.terminal:
						nepisodes_solved += 1

					(state, state_with_history, next_state_with_history) = self.initializeGameTraining(self.simulatorTrain)

				if gamePlayer is not self.simulatorEvaluate.target:
					networkInput = self.getNetworkInput(i, state_with_history, len(self.simulatorEvaluate.bots))
				else:
					networkInput = None

				action = gamePlayer.takeAction(networkInput, epsilon)
				next_state = self.simulatorTrain.step(action, gamePlayer)

				self.append_to_hist(
					next_state_with_history[i],
					rgb2gray(next_state.pob[i]).reshape(self.opt.state_siz)
				)

				if gamePlayer is not self.simulatorTrain.target:
					transitionTables[i].add(
						networkInput.reshape(-1),
						transitionTables[i].one_hot_action(next_state.action),
						self.getNetworkInput(i, next_state_with_history, len(self.simulatorEvaluate.bots)).reshape(-1),
						next_state.reward,
						next_state.terminal
					)

				if next_state.terminal:
					for j, bot in enumerate(self.simulatorTrain.bots):
						if bot is not gamePlayer:
							transitionTables[j].rewards[transitionTables[j].top - 1] += next_state.reward / (len(self.simulatorTrain.bots) - 1)

				state_with_history[i] = np.copy(next_state_with_history[i])
				state = next_state

				if gamePlayer is not self.simulatorTrain.target:
					lastLossValues[i] = gamePlayer.train(transitionTables[i])

				self.drawGame(state)
				epi_step += 1
				step += 1

	def getNetworkInput(self, playerId, state_with_history, numberOfBots):
		networkInput = np.zeros((numberOfBots, self.opt.hist_len * self.opt.state_siz))

		networkInput[0] = state_with_history[playerId].reshape(1, self.opt.hist_len * self.opt.state_siz)

		j = 1
		for i in range(0, numberOfBots):
			if i != playerId:
				networkInput[j] = state_with_history[i].reshape(1, self.opt.hist_len * self.opt.state_siz)
				j += 1

		return networkInput.reshape(1, numberOfBots * self.opt.hist_len * self.opt.state_siz)

	def printProgess(self, step, nepisodes, nepisodes_to_run, lastLossValues):
		self.endProgress = time.time()
		if step % 50 == 0:
			print("Step: {0}, {1} episodes ouf of {2} - {3:.2f}% | Time: {4:.4f}s".format(
					step,
					nepisodes,
					nepisodes_to_run,
					(nepisodes / nepisodes_to_run) * 100,
					self.endProgress - self.startProgress
				)
			)

			print("Loss values:")
			for i in range(0, lastLossValues.shape[0]):
				print("\tBot {}: {}".format(i, lastLossValues[i]))

			self.startProgress = time.time()


	def getTransitionTables(self):
		maxlen = self.opt.minibatch_size * 4

		transitionTables = []
		for i in range(0, (len(self.gamePlayers) - 1)):
			transitionTables.append(
				TransitionTable(
					self.opt.state_siz,
					self.opt.act_num,
					self.opt.hist_len,
					self.opt.minibatch_size,
					self.opt.number_of_bots,
					maxlen
				)
			)

		return transitionTables

	def checkTrainingProgress(self, nepisodes):
		if (nepisodes % 1000) == 0:
			accuracy = self.evaluate(30)
			print("Number of episodes: {}".format(nepisodes))
			print("\tAccuracy: {0:.2f}%".format(accuracy))


	def drawGame(self, state):
		if self.opt.disp_on:
			if self.win_all is None:
				plt.subplot(121)
				self.win_all = plt.imshow(state.screen)
				plt.subplot(122)
				self.win_pob = plt.imshow(state.pob[0])
			else:
				self.win_all.set_data(state.screen)
				self.win_pob.set_data(state.pob[0])
				plt.pause(self.opt.disp_interval)
				plt.draw()

	def getAccuracy(self, nepisodes, nepisodes_solved):
		return (nepisodes_solved / nepisodes) * 100

	def append_to_hist(self, state, obs):
		"""
		Add observation to the state.
		"""
		for i in range(state.shape[0]-1):
			state[i, :] = state[i+1, :]

		state[-1, :] = obs

	def initializeGameTraining(self, simulator):
		state = simulator.newGame(self.opt.tgt_y, self.opt.tgt_x)

		numberOfGamePlayers = len(self.gamePlayers)

		state_with_history = np.zeros(
			(numberOfGamePlayers, self.opt.hist_len, self.opt.state_siz)
		)

		for i, gamePlayer in enumerate(self.gamePlayers):
			self.append_to_hist(
				state_with_history[i],
				rgb2gray(state.pob[i]).reshape(self.opt.state_siz)
			)

		next_state_with_history = np.copy(state_with_history)

		self.drawGame(state)

		return (state, state_with_history, next_state_with_history)
