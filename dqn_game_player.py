from game_player import GamePlayer
import numpy as np
from network import Network
import tensorflow as tf
import q_loss
from random import randrange

class DQNGamePlayer(GamePlayer):
	def __init__(self, playerId):
		super(DQNGamePlayer, self).__init__(playerId)
		self.network = Network("DQN__" + self.id)

		x = tf.placeholder(
			tf.float32,
			shape=(None, self.opt.number_of_bots * self.opt.hist_len * self.opt.state_siz),
			name="x_" + self.id
		)
		u = tf.placeholder(
			tf.float32, shape=(self.opt.minibatch_size, self.opt.act_num),
			name="u_" + self.id
		)
		ustar = tf.placeholder(
			tf.float32, shape=(self.opt.minibatch_size, self.opt.act_num),
			name="ustar_" + self.id
		)
		xn = tf.placeholder(
			tf.float32, shape=(None, self.opt.number_of_bots * self.opt.hist_len*self.opt.state_siz),
			name="xn_" + self.id
		)
		reward = tf.placeholder(
			tf.float32, shape=(self.opt.minibatch_size, 1),
			name="reward_" + self.id
		)
		terminal = tf.placeholder(
			tf.float32, shape=(self.opt.minibatch_size, 1),
			name="terminal_" + self.id
		)

		self.Q_s = self.network.getNetwork(x)
		self.Q_s_n = self.network.getNetwork(xn)

		self.loss = q_loss.Q_loss(
			self.Q_s,
			u,
			self.Q_s_n,
			ustar,
			reward,
			terminal
		)

		self.global_step_tensor = tf.Variable(10, trainable=False, name='global_step')
		optimizer = tf.train.AdamOptimizer(learning_rate=0.0005)
		self.train_op = optimizer.minimize(
			loss=self.loss,
			global_step=tf.train.get_global_step()
		)

	def setSession(self, session):
		self.session = session

	def takeAction(self, state=None, randomProbability=0):
		if np.random.uniform() * 100 <= randomProbability:
			return randrange(self.opt.act_num)

		action_tf = tf.argmax(input=self.Q_s, axis=1)

		action = self.session.run(
			action_tf,
			feed_dict={"x_{}:0".format(self.id): state}
		)

		return action[0]

	def train(self, transitionTable):
		state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = transitionTable.sample_minibatch()

		action_batch_next_tf = tf.argmax(input=self.Q_s_n, axis=1)
		action_batch_next_not_onehot = self.session.run(
			action_batch_next_tf,
			feed_dict = {"xn_{}:0".format(self.id): next_state_batch}
		)

		action_batch_next = np.zeros((self.opt.minibatch_size, self.opt.act_num))

		for i in range(len(action_batch_next_not_onehot)):
			action_batch_next[i] = transitionTable.one_hot_action(action_batch_next_not_onehot[i])

		self.session.run(
			self.train_op,
			feed_dict = {
				"x_{}:0".format(self.id): state_batch,
				"u_{}:0".format(self.id): action_batch,
				"ustar_{}:0".format(self.id): action_batch_next,
				"xn_{}:0".format(self.id): next_state_batch,
				"reward_{}:0".format(self.id): reward_batch,
				"terminal_{}:0".format(self.id): terminal_batch
			}
		)

		loss_value = self.session.run(
			self.loss,
			feed_dict = 
			{
				"x_{}:0".format(self.id): state_batch,
				"u_{}:0".format(self.id): action_batch,
				"ustar_{}:0".format(self.id): action_batch_next,
				"xn_{}:0".format(self.id): next_state_batch,
				"reward_{}:0".format(self.id): reward_batch,
				"terminal_{}:0".format(self.id): terminal_batch
			}
		)

		globalStep = self.session.run(tf.train.get_global_step())

		if globalStep % 500 == 0:
			print("Saving...{}".format(self.id))
			tf.train.Saver().save(self.session, "./model/")
			print("\tDone")

		return loss_value

