from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file

# List ALL tensors.
print_tensors_in_checkpoint_file(
	file_name='./model/',
	tensor_name='',
	all_tensors=True
)